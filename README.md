# Vue CLI Plugin - Publish Utils

## About
This is a [plugin](https://cli.vuejs.org/core-plugins/) written for Vue CLI v3.x (Vue v2.x), which adds utilities to an existing Vue project so that it can be published as a library. The project can be built/served as a stand-alone application just like before, while the plugin adds additional scripts to enforce [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary), [versioning](https://semver.org/) and configurations to build and publish a library. The plugin currently supports CI/CD-based publishing for repositories hosted on Bitbucket Cloud.

## Setup
Install the plugin (When hosted on a private registry)
```
vue add @deltax/publish-utils
```
  
Alternatively, install using the repository URL:
```
npm install --save-dev git+https://bitbucket.org/amberramesh/vue-cli-plugin-publish-utils.git
```

And invoke the plugin:
```
vue invoke @deltax/publish-utils
```

Learn more about [adding plugins](https://cli.vuejs.org/guide/plugins-and-presets.html#plugins) to a Vue project.

The plugin's generator will modify source files and install tools required for packaging your application. Ensure that the files modified are reviewed, after installing the plugin and before committing any changes. It is recommended to invoke the plugin on a fresh project instead of a mature application.

This plugin is an initial version supporting **[Bitbucket Pipelines](https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/)**. After pushing the files added by the plugin, Pipelines need to be enabled on the Bitbucket Cloud repository.
For a first-time configuration, the following workspace/repository variables should be configured:

| Name | Description |
| -------- | --------- |
| CLIENT_ID | Bitbucket OAuth Consumer Key. Required. |
| CLIENT_SECRET | Bitbucket OAuth Consumer Secret. Required. |
| NPM_PUBLISH_REGISTRY | Complete URL of an NPM registry. Required if publishing to a private registry. |
| NPM_SCOPE | User or organization scope, if the package is using one. Optional. |
| NPM_TOKEN | Access token obtained through [npm token create](https://docs.npmjs.com/cli/v6/commands/npm-token). Required for publishing package. Can be substituted with NPM_USERNAME, NPM_PASSWORD and NPM_EMAIL. |
| NPM_USERNAME | Username for the NPM Registry. |
| NPM_PASSWORD | Account password. |
| NPM_EMAIL | E-mail of the user. |
| GIT_AUTHOR_NAME | Optional but recommended. |
| GIT_AUTHOR_EMAIL | Optional but recommended. |
| GIT_COMMITTER_NAME | Optional but recommended. |
| GIT_COMMITTER_EMAIL | Optional but recommended. |

Bitbucket consumer key and secret can be obtained from **Workspace Settings > OAuth consumers** tab by adding or using an existing consumer.  
Ensure that repository:write permission is enabled for this consumer.  
Many of these variables are used by the [NPM](https://github.com/semantic-release/npm) and [Git](https://github.com/semantic-release/git) plugins for [semantic-release](https://github.com/semantic-release/semantic-release).

After enabling Pipelines, changes pushed to master, feature/\* or release/\* branches will be automatically published as a package.  
Different distribution channels are used depending on the branch:  
**master**: Considered as the release branch and uses the default distribution channel.  
**feature/\***: Prerelease branches that use alpha channel.  
**release/\***: Prerelease branches that use beta channel.

This plugin uses [Rollup.js](https://rollupjs.org/guide/en/#overview) for bundling your code with its dependencies as an ES module. If you're building a [Vue plugin](https://vuejs.org/v2/guide/plugins.html) (not a CLI plugin!) that can be used in other Vue applications, just import it into the application using:

```
import MyPlugin from '<your-package-name>';

Vue.use(MyPlugin);
```
And your plugin will be usable throughout the application!

If your plugin uses Vuex, just update the last line to:
```
Vue.use(MyPlugin, { store });
// This is the store instance passed to the root Vue instance.
```

Sometimes, your Vue plugin may require additional configuration not present in the default [config file](https://bitbucket.org/amberramesh/vue-cli-plugin-publish-utils/src/master/generator/template/rollup.config.js). In such cases, you might want to install [plugins for Rollup](https://rollupjs.org/guide/en/#using-plugins) (yes, plugins everywhere) or fine-tune the ones in your project to get the desired build output.

Currently, there is no support for exporting multiple named modules (ideal when building a multi-component library like [Vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/#getting-started)),  or exporting CJS modules or IIFE, but they may be added soon.

## Contribution Guidelines
This plugin is an early preview and not open for contribution yet. It may eventually be available for open source contribution on Github. Contact me for any additional details on [amberramesh@gmail.com](mailto:amberramesh@gmail.com)
