const fs = require('fs');
const { EOL } = require('os');

module.exports = (api) => {
  api.extendPackage({
    main: 'lib/index.esm.js',
    scripts: {
      'build:lib': 'rm -rf lib && rollup -c',
      commit: 'git-cz',
      prepare: 'npm run build:lib',
    },
    devDependencies: {
      '@commitlint/cli': '^11.0.0',
      '@commitlint/config-conventional': '^11.0.0',
      '@rollup/plugin-alias': '^3.1.1',
      '@rollup/plugin-babel': '^5.2.2',
      '@rollup/plugin-commonjs': '^16.0.0',
      '@rollup/plugin-json': '^4.1.0',
      '@rollup/plugin-node-resolve': '^10.0.0',
      '@rollup/plugin-replace': '^2.3.4',
      '@semantic-release/changelog': '^5.0.1',
      '@semantic-release/git': '^9.0.0',
      commitizen: '^4.2.2',
      'cz-conventional-changelog': '^3.3.0',
      husky: '^4.3.0',
      rollup: '^2.33.3',
      'rollup-plugin-node-builtins': '^2.1.2',
      'rollup-plugin-node-globals': '^1.4.0',
      'rollup-plugin-postcss': '^3.1.8',
      'rollup-plugin-smart-asset': '^2.1.0',
      'rollup-plugin-vue': '^5.0.0',
      'semantic-release': '^17.3.0',
    },
  });

  api.onCreateComplete(() => {
    // Update gitignore to ignore files generated in /lib directory
    const gitignore = api.resolve('.gitignore');
    const contentMain = fs.readFileSync(gitignore, { encoding: 'utf-8' });
    const lines = contentMain.split(/\r?\n/g);

    // Avoid duplicate entry if /lib is already ignored
    if (lines.some((line) => /^\/lib$/.test(line))) return;
    // Search for line containing /dist entry, or a blank line
    const entryIndex = lines.findIndex((line) => /^(\/dist)?$/.test(line));
    lines[entryIndex] += `${EOL}/lib`;

    fs.writeFileSync(gitignore, lines.join(EOL), { encoding: 'utf-8' });
  });

  api.render('./template');
};
