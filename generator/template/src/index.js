import CustomPlugin from './App.vue';
<%_ if (rootOptions.vuex) { _%>
import store from './store';
<%_ } _%>

if (!CustomPlugin.name) {
  throw new Error('Root instance used within a plugin must be named.');
}
let pluginName = CustomPlugin.name;

export default {
  install(Vue, options) {
    if (options && typeof options.name === 'string') {
      pluginName = options.name;
    }
<%_ if (rootOptions.vuex) { _%>
    if (!options || !options.store) {
      throw new Error(`Vuex is required to use the ${pluginName} plugin. Please provide 'store' within options.`);
    }

    if (typeof store.modules === 'object') {
      Object.keys(store.modules).forEach((module) => {
        options.store.registerModule(module, store.modules[module]);
      });
    }  
<%_ } _%>

    // Configure additional options for the plugin here.

    Vue.component(pluginName, CustomPlugin);
  },
};
